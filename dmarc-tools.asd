(asdf:defsystem :dmarc-tools
	:version "0.0.1"
	:components ((:module "src"
			      :components ((:file "packages")
					   (:file "dmarc-record" :depends-on ("packages"))
					   (:file "record-parser" :depends-on ("packages" "dmarc-record")))))

	:depends-on ("cl-lex" "yacc" "alexandria"))
