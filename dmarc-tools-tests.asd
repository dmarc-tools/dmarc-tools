(asdf:defsystem :dmarc-tools-tests
  :depends-on (:dmarc-tools :fiveam)
  :version "0.0.1"
  :components ((:module "t"
			:components ((:file "record-parser-tests"))))
  :in-order-to ((asdf:test-op (asdf:load-op "dmarc-tools-tests")))
  :perform (asdf:test-op :after (op c)
			 (funcall (intern (string '#:run!) :fiveam)
					    :dmarc-tools)))
