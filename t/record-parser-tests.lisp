;; FIXME: break this out when I have more than one test file
(defpackage dmarc-tools-tests
  (:use #:cl #:dmarc-tools #:fiveam))

(in-package #:dmarc-tools-tests)

(def-suite :dmarc-tools :description "tests for dmarc-tools")
(in-suite :dmarc-tools)

(defparameter *test-string* "v=DMARC1; p=reject; rua=mailto:dmarc@dianomi.com; ruf=mailto:dmarc@dianomi.com; rf=afrf; pct=100;")

(defparameter *test-record* (record-from-string *test-string*))

(test record-from-string
  (is (equal "DMARC1" (dmarc-record-v *test-record*)))
  (is (equal "reject" (dmarc-record-p *test-record*)))
  (is (equal "mailto:dmarc@dianomi.com" (dmarc-record-rua *test-record*)))
  (is (equal "mailto:dmarc@dianomi.com" (dmarc-record-ruf *test-record*)))
  (is (equal "afrf" (dmarc-record-rf *test-record*)))
  (is (equal 100 (dmarc-record-pct *test-record*)))
  )
  
