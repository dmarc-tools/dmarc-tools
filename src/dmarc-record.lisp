(in-package :dmarc-tools)

(defclass dmarc-record ()
  ((adkim 
    :initarg :adkim
    :accessor dmarc-record-adkim
    :initform "r"
    :documentation "Indicates whether or not strict DKIM identifier alignment is required by the Domain Owner")
   (aspf
    :initarg :aspf
    :accessor dmarc-record-aspf
    :initform "r"
    :documentation "Indicates whether or not strict SPF identifier alignment is required by the Domain Owner")
   (p
    :initarg :p
    :accessor dmarc-record-p
    :initform (error "p value is required")
    :documentation "Indicates the policy to be enacted by the Receiver at the request of the Domain Owner")
   (pct
    :initarg :pct
    :accessor dmarc-record-pct
    :initform 100
    :documentation "Percentage of messages from the DNS domain's mail stream to which the DMARC mechanism is to be applied.")
   (rf
    :initarg :rf
    :accessor dmarc-record-rf
    :initform "afrf"
    :documentation "Format to be used for message-specific forensic information reports")
   (ri
    :initarg :ri
    :accessor dmarc-record-ri
    :initform 86400
    :documentation "Interval requested between aggregate reports")
   (rua
    :initarg :rua
    :accessor dmarc-record-rua
    :initform nil
    :documentation "Addresses to which aggregate feedback is to be sent")
   (ruf
    :initarg :ruf
    :accessor dmarc-record-ruf
    :initform nil
    :documentation "Addresses to which message-specific forensic information is to be reported")
   (sp
    :initarg :sp
    :accessor dmarc-record-sp
    :documentation "Requested Mail Receiver policy for subdomains")
   (v
    :initarg :v
    :accessor dmarc-record-v
    :initform (error "v is required")
    :documentation "Version")))
