
(defpackage :dmarc-tools
  (:use :common-lisp
	:cl-lex
	:yacc
	:alexandria)
  (:export :record-from-string
	   :dmarc-record-v
	   :dmarc-record-adkim
	   :dmarc-record-aspf
	   :dmarc-record-p
	   :dmarc-record-pct
	   :dmarc-record-rf
	   :dmarc-record-ri
	   :dmarc-record-rua
	   :dmarc-record-ruf
	   :dmarc-record-sp))
